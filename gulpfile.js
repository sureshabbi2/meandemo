"use strict";

var gulp=require("gulp");
var nodemon=require("gulp-nodemon");
var jshint=require("gulp-jshint");

var watchFiles={
  serverJS: ['gulpfile.js','server.js']
};

gulp.task('nodemon', function(){
  return nodemon({
    verbose:true,
    script: 'server.js',
    ext:'js',
    watch: watchFiles.serverJS,
  });
});

gulp.task('jshint',function(){
  return gulp.src(watchFiles.serverJS).pipe(jshint()).pipe(jshint.reporter('jshint-stylish'))
  .pipe(jshint.reporter('fail'));
});

gulp.task('watch', function(){
  gulp.watch(watchFiles.serverJS, ['jshint']);
});

gulp.task('lint', ['jshint']);

gulp.task('default',['lint','nodemon','watch']);
